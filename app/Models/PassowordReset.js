"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class PassowordReset extends Model {
  static boot() {
    super.boot();
    // ...

    this.addHook("beforeCreate", async model => {
      model.token = await str_random(25);

      const expire_at = new Date();
      expire_at.setMinutes(expire_at.getMinutes() + 30);
      model.expires_at = expire_at;
    });
  }

  // Formata os valores para padrao mysql
  static get dates() {
      return ['created_at', 'updated_at', 'expires_at']
  }


}

module.exports = PassowordReset;
