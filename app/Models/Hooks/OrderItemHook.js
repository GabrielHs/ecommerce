"use strict";

const OrderItemHook = (exports = module.exports = {});

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Product = use("App/Models/Product");

OrderItemHook.updateSubtotal = async modelInstance => {
  let product = await Product.find(modelInstance.product_id);
  modelInstance.subtotal = model.quantity * product.price;
};
